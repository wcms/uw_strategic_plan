<?php

/**
 * @file
 * uw_strategic_plan.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function uw_strategic_plan_filter_default_formats() {
  $formats = array();

  // Exported format: Homepage Body.
  $formats['homepage_body'] = array(
    'format' => 'homepage_body',
    'name' => 'Homepage Body',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  // Exported format: Homepage Centre Text.
  $formats['homepage_centre_text'] = array(
    'format' => 'homepage_centre_text',
    'name' => 'Homepage Centre Text',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  // Exported format: Progress dashboards Text.
  $formats['key_indicators_text'] = array(
    'format' => 'key_indicators_text',
    'name' => 'Progress dashboards Text',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  return $formats;
}
