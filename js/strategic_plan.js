/**
 * @file
 */

(function ($) {
  Drupal.behaviors.myfunction = {
    attach: function (context, settings) {

      // Function to parse a provided URL's query string.
      function get_query(url){
        var qs = url.substring(url.indexOf('?') + 1).split('&');
        for (var i = 0, result = {}; i < qs.length; i++) {
          qs[i] = qs[i].split('#');
          qs[i] = qs[i][0].split('=');
          result[qs[i][0]] = decodeURIComponent(qs[i][1]);
        }
        return result;
      }

      // Get the node id (nid) and revision (vid) of the current node.
      var nid = Drupal.settings.uw_strategic_plan.nid;
      var vid = Drupal.settings.uw_strategic_plan.vid;

      // Handle clicks for the tab navigation.
      $(document).on('click', 'a[href$="#tabs-nav"]', function () {
        // Determine what tab is needed.
        tab = get_query($(this).attr('href')).tab;
        // don't need to do anything if this is already the active tab.
        if (!$('#tabs #tab-' + tab + '.active').length) {
          // Make every tab inactive.
          $(this).closest('#tabs-nav').find('li.active').removeClass('active');
          // Make the selected tab active.
          $(this).closest('li').addClass('active');
          // Hide the currently active tab content area.
          $('#tabs > div.active').removeClass('active');
          // Define the selected tab content area.
          $tab_content = $('#tabs > #tab-' + tab);
          // Make the selected tab content area active.
          $tab_content.addClass('active');
          // Load the tab content if necessary.
          if (!$tab_content.hasClass('loaded')) {
            path = Drupal.settings.basePath + 'ajax/stratplan/?nid=' + nid + '&vid=' + vid + '&tab=' + tab;
            $.get(path,function (data) {
              $tab_content.addClass('loaded').html(data);
              $('#tab-all-nav ul a:first', $tab_content).click();
              prepExpandable();
            }).error(function () {
              // AJAX isn't working, fall back to old school behaviour.
              $('a[href$="#tabs-nav"]').unbind();
            });
          }
        }

        // Handle links within Progress dashboards.
        if (tab == 'key') {
          tableau = get_query($(this).attr('href')).tableau;
          if (!tableau) {
            // No tableau required, show "buttons".
            $('#tab-key .tableau.active').removeClass('active');
            $('#key-indicators').addClass('active');
          }
else {
            $tableau_content = $('#tab-key .tableau-' + tableau);
            // If this tableau DIV doesn't exist, create it.
            if (!$tableau_content.length) {
              $('#tab-key').append('<div class="tableau tableau-' + tableau + '"></div>');
              $tableau_content = $('#tab-key .tableau-' + tableau);
            }
            // don't need to do anything if this is already the active tableau.
            if (!$tableau_content.hasClass('active')) {
              // Make the "buttons" inactive.
              $('#key-indicators.active').removeClass('active');
              // Make any existing active tableaus inactive.
              $('#tab-key .tableau.active').removeClass('active');
              // Make the selected tableau active.
              $tableau_content.addClass('active');
              // Load the tab content if necessary.
              if (!$tableau_content.hasClass('loaded')) {
                path = Drupal.settings.basePath + 'ajax/stratplan/?nid=' + nid + '&vid=' + vid + '&tab=' + tab + '&tableau=' + tableau;
                $.get(path,function (data) {
                  $tableau_content.addClass('loaded').html(data);
                  prepExpandable();
                }).error(function () {
                  // AJAX isn't working, fall back to old school behaviour.
                  $('a[href$="#tabs-nav"]').unbind();
                });
              }
            }
          }
        }

        // Handle links within "all indicators".
        if (tab == 'all') {
          $('#tab-all-nav a').removeClass('active');
          $(this).addClass('active');
          tableau = get_query($(this).attr('href')).tableau;
          if (tableau === '0') {
            // When tabs are switched, the first tableau is loaded; need to add the active class to its menu item.
            $('#tab-all-nav a:first').addClass('active');
          }
          if (!tableau) {
            // No tableau required, hide any visible.
            $('#tab-all-display.active, #tab-all-display .active').removeClass('active');
          }
else {
            $tableau_content = $('#tab-all-display .tableau-' + tableau);
            // If this tableau DIV doesn't exist, create it.
            if (!$tableau_content.length) {
              $('#tab-all-display').append('<div class="tableau tableau-' + tableau + '"></div>');
              $tableau_content = $('#tab-all-display .tableau-' + tableau);
            }
            // don't need to do anything if this is already the active tableau.
            if (!$tableau_content.hasClass('active')) {
              // Make any existing active tableaus inactive.
              $('#tab-all-display .tableau.active').removeClass('active');
              // Make the selected tableau active.
              $tableau_content.addClass('active');
              // Make sure the container knows there's an active tableau.
              $('#tab-all-display').addClass('active');
              // Load the tab content if necessary.
              if (!$tableau_content.hasClass('loaded')) {
                path = Drupal.settings.basePath + 'ajax/stratplan/?nid=' + nid + '&vid=' + vid + '&tab=' + tab + '&tableau=' + tableau;
                $.get(path,function (data) {
                  $tableau_content.addClass('loaded').html(data);
                  prepExpandable();
                }).error(function () {
                  // AJAX isn't working, fall back to old school behaviour.
                  $('a[href$="#tabs-nav"]').unbind();
                });
              }
            }
          }
        }

        // Stop the link from working the non-AJAX way.
        return false;
      });

      // Custom expandable/collapsible regions code because we have tabs and AJAX
      $(document).on('click', '#content .field-item > .expandable > h2:first-child', function () { // .node-type-uw-web-page.
        $expandable = $(this).parent();
        if ($expandable.hasClass('expanded')) {
          $expandable.removeClass('expanded');
          $('.expandable-content',$expandable).prev().addClass('last-visible');
        }
else {
          $expandable.addClass('expanded');
          $('.expandable-content',$expandable).prev().removeClass('last-visible');
        }
      })

      // Function to make the expandable/collapsible areas work, which should be called by any JavaScript that might introduce a new one to the page.
      function prepExpandable() {
        $('#content .field-item > .expandable > h2:first-child:not(:has(button))').wrapInner('<button>');
        // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins.
        $('#content .field-item > .expandable > .expandable-content').prev().addClass('last-visible');
        $('#content .field-item > .expandable > .expandable-content :last-child').addClass('last-visible');
        // Areas that are "containers" for E/C sets that might need their own E/C-all buttons. Edit as needed.
        $('.tabs-add, #tab-all-display .tableau').each(function (index, element) {
          // If there is more than one expandable region in the area, add expand/collapse all functions.
          if ($('.field-item > .expandable', this).length > 1) {
            // don't set up controls if they already exist.
            if (!$('.expandable-controls', this).length) {
              $('.field-item > .expandable:first', this).before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');
              $('.expandable-controls .expand-all', this).click(function () {
                // Rather than recreate clicking logic here, just click the affected items.
                $('.expandable:not(.expanded) h2:first-child', element).click();
              });
              $('.expandable-controls .collapse-all', this).click(function () {
                // Rather than recreate clicking logic here, just click the affected items.
                $('.expandable.expanded h2:first-child', element).click();
              });
            }
          }
        });
      }
      prepExpandable();

      // Do things when the anchor changes.
      window.onhashchange = uw_fdsu_anchors;
      // Trigger the event on page load in case there is already an anchor.
      uw_fdsu_anchors();

      // Function to do things when the anchor changes.
      function uw_fdsu_anchors() {
        if (location.hash) {
          // Check if there is an ID with this name.
          if ($(location.hash).length) {
            // If it's in an unexpanded expandable content area, expand the expandable content area.
            $(location.hash,'.expandable:not(.expanded)').closest('.expandable').find('h2:first-child').click();
            // Scroll to the ID, taking into account the toolbar if it exists
            $('html, body').scrollTop($(location.hash).offset().top - $('#toolbar').height()); // "html" works in Firefox, "body" works in Chrome/Safari.
          }
        }
      }

    }
  }
}
)
(jQuery);
