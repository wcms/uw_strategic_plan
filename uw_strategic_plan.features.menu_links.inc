<?php

/**
 * @file
 * uw_strategic_plan.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_strategic_plan_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_themes:admin/structure/taxonomy/uw_tax_themes.
  $menu_links['menu-site-manager-vocabularies_themes:admin/structure/taxonomy/uw_tax_themes'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_tax_themes',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Themes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_themes:admin/structure/taxonomy/uw_tax_themes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Themes');

  return $menu_links;
}
