<?php

/**
 * @file
 * uw_strategic_plan.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_strategic_plan_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_strategic_plan_strongarm_alter(&$data) {
  if (isset($data['page_title_default'])) {
    $data['page_title_default']->value = '[current-page:page-title] | Strategic plan'; /* WAS: '[current-page:page-title] | [site:name]' */
  }
  if (isset($data['responsive_menu_combined_settings'])) {
    $data['responsive_menu_combined_settings']->value['devel'] = array(
      'friendly_name' => 'Development',
      'enabled' => 0,
      'weight' => 0,
      'id' => 'devel',
      'pid' => '',
      'depth' => '',
    ); /* WAS: '' */
    $data['responsive_menu_combined_settings']->value['features'] = array(
      'friendly_name' => 'Features',
      'enabled' => 0,
      'weight' => 1,
      'id' => 'features',
      'pid' => '',
      'depth' => '',
    ); /* WAS: '' */
    $data['responsive_menu_combined_settings']->value['main-menu']['enabled'] = 0; /* WAS: 1 */
    $data['responsive_menu_combined_settings']->value['menu-audience-menu']['enabled'] = 0; /* WAS: 1 */
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_strategic_plan_image_default_styles() {
  $styles = array();

  // Exported image style: theme_icon__home_page_.
  $styles['theme_icon__home_page_'] = array(
    'label' => 'Theme icon (home page)',
    'effects' => array(
      2 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 90,
          'height' => 90,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: theme_icon__landing_page_.
  $styles['theme_icon__landing_page_'] = array(
    'label' => 'Theme icon (landing page)',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 200,
          'height' => 200,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_strategic_plan_node_info() {
  $items = array(
    'uw_ct_sp_home_page' => array(
      'name' => t('Home page'),
      'base' => 'node_content',
      'description' => t('Text for the home page.'),
      'has_title' => '1',
      'title_label' => t('Home page title'),
      'help' => '',
    ),
    'uw_ct_theme_page' => array(
      'name' => t('Theme page'),
      'base' => 'node_content',
      'description' => t('Contents of the theme landing page. There must be one for each theme.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_strategic_plan_paragraphs_info() {
  $items = array(
    'indicators_content' => array(
      'name' => 'Indicators content',
      'bundle' => 'indicators_content',
      'locked' => '1',
    ),
    'indicators_header' => array(
      'name' => 'Indicators header',
      'bundle' => 'indicators_header',
      'locked' => '1',
    ),
    'indicators_tableau' => array(
      'name' => 'Indicators tableau',
      'bundle' => 'indicators_tableau',
      'locked' => '1',
    ),
    'key_indicator' => array(
      'name' => 'Progress dashboard',
      'bundle' => 'key_indicator',
      'locked' => '1',
    ),
    'theme_tab' => array(
      'name' => 'Theme tab',
      'bundle' => 'theme_tab',
      'locked' => '1',
    ),
  );
  return $items;
}
