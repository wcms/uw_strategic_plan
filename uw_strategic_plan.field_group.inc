<?php

/**
 * @file
 * uw_strategic_plan.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_strategic_plan_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_tabs|node|uw_ct_theme_page|form';
  $field_group->group_name = 'group_additional_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_theme_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional tabs',
    'weight' => '6',
    'children' => array(
      0 => 'field_tab',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_additional_tabs|node|uw_ct_theme_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_key_indicators|node|uw_ct_theme_page|form';
  $field_group->group_name = 'group_key_indicators';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_theme_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Progress dashboards',
    'weight' => '5',
    'children' => array(
      0 => 'field_key_indicator',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Progress dashboards',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_key_indicators|node|uw_ct_theme_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_see_all_indicators|node|uw_ct_theme_page|form';
  $field_group->group_name = 'group_see_all_indicators';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_theme_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'All indicators tab items',
    'weight' => '7',
    'children' => array(
      0 => 'field_indicator',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_see_all_indicators|node|uw_ct_theme_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_file|node|uw_ct_sp_home_page|form';
  $field_group->group_name = 'group_upload_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_sp_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload File',
    'weight' => '7',
    'children' => array(
      0 => 'field_upload_file_home',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload File',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-file field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_file|node|uw_ct_sp_home_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_file|node|uw_ct_theme_page|form';
  $field_group->group_name = 'group_upload_file';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_theme_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload File',
    'weight' => '4',
    'children' => array(
      0 => 'field_upload_file_theme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload File',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-file field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_file|node|uw_ct_theme_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_image|node|uw_ct_sp_home_page|form';
  $field_group->group_name = 'group_upload_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_sp_home_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Image',
    'weight' => '6',
    'children' => array(
      0 => 'field_upload_image_home',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload Image',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_image|node|uw_ct_sp_home_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_image|node|uw_ct_theme_page|form';
  $field_group->group_name = 'group_upload_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_theme_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Image',
    'weight' => '3',
    'children' => array(
      0 => 'field_image_theme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload Image',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-upload-image field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_image|node|uw_ct_theme_page|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional tabs');
  t('All indicators tab items');
  t('Progress dashboards');
  t('Upload File');
  t('Upload Image');

  return $field_groups;
}
