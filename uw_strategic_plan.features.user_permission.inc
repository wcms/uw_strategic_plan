<?php

/**
 * @file
 * uw_strategic_plan.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_strategic_plan_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_sp_home_page content'.
  $permissions['create uw_ct_sp_home_page content'] = array(
    'name' => 'create uw_ct_sp_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_ct_theme_page content'.
  $permissions['create uw_ct_theme_page content'] = array(
    'name' => 'create uw_ct_theme_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_sp_home_page content'.
  $permissions['delete any uw_ct_sp_home_page content'] = array(
    'name' => 'delete any uw_ct_sp_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_theme_page content'.
  $permissions['delete any uw_ct_theme_page content'] = array(
    'name' => 'delete any uw_ct_theme_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_sp_home_page content'.
  $permissions['delete own uw_ct_sp_home_page content'] = array(
    'name' => 'delete own uw_ct_sp_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_theme_page content'.
  $permissions['delete own uw_ct_theme_page content'] = array(
    'name' => 'delete own uw_ct_theme_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_tax_themes'.
  $permissions['delete terms in uw_tax_themes'] = array(
    'name' => 'delete terms in uw_tax_themes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_sp_home_page content'.
  $permissions['edit any uw_ct_sp_home_page content'] = array(
    'name' => 'edit any uw_ct_sp_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_theme_page content'.
  $permissions['edit any uw_ct_theme_page content'] = array(
    'name' => 'edit any uw_ct_theme_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_sp_home_page content'.
  $permissions['edit own uw_ct_sp_home_page content'] = array(
    'name' => 'edit own uw_ct_sp_home_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_theme_page content'.
  $permissions['edit own uw_ct_theme_page content'] = array(
    'name' => 'edit own uw_ct_theme_page content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_tax_themes'.
  $permissions['edit terms in uw_tax_themes'] = array(
    'name' => 'edit terms in uw_tax_themes',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_sp_home_page revision log entry'.
  $permissions['enter uw_ct_sp_home_page revision log entry'] = array(
    'name' => 'enter uw_ct_sp_home_page revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter uw_ct_theme_page revision log entry'.
  $permissions['enter uw_ct_theme_page revision log entry'] = array(
    'name' => 'enter uw_ct_theme_page revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page authored by option'.
  $permissions['override uw_ct_sp_home_page authored by option'] = array(
    'name' => 'override uw_ct_sp_home_page authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page authored on option'.
  $permissions['override uw_ct_sp_home_page authored on option'] = array(
    'name' => 'override uw_ct_sp_home_page authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page promote to front page option'.
  $permissions['override uw_ct_sp_home_page promote to front page option'] = array(
    'name' => 'override uw_ct_sp_home_page promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page published option'.
  $permissions['override uw_ct_sp_home_page published option'] = array(
    'name' => 'override uw_ct_sp_home_page published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page revision option'.
  $permissions['override uw_ct_sp_home_page revision option'] = array(
    'name' => 'override uw_ct_sp_home_page revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_sp_home_page sticky option'.
  $permissions['override uw_ct_sp_home_page sticky option'] = array(
    'name' => 'override uw_ct_sp_home_page sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page authored by option'.
  $permissions['override uw_ct_theme_page authored by option'] = array(
    'name' => 'override uw_ct_theme_page authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page authored on option'.
  $permissions['override uw_ct_theme_page authored on option'] = array(
    'name' => 'override uw_ct_theme_page authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page promote to front page option'.
  $permissions['override uw_ct_theme_page promote to front page option'] = array(
    'name' => 'override uw_ct_theme_page promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page published option'.
  $permissions['override uw_ct_theme_page published option'] = array(
    'name' => 'override uw_ct_theme_page published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page revision option'.
  $permissions['override uw_ct_theme_page revision option'] = array(
    'name' => 'override uw_ct_theme_page revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_theme_page sticky option'.
  $permissions['override uw_ct_theme_page sticky option'] = array(
    'name' => 'override uw_ct_theme_page sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
