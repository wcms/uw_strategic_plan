/**
 * @file
 * This file is used/requested by the 'Styles' button.
 *
 * 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 *
 * Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license.
 */

CKEDITOR.addStylesSet('custom',
[
  /* Block Styles */

  // These styles are already available in the "Format" combo, so they are
  // not needed here by default. You may enable them to avoid placing the
  // "Format" combo in the toolbar, maintaining the same features.
  { name : 'Paragraph', element : 'p' },
  { name : 'Heading 3', element : 'h3' },
  { name : 'Heading 4', element : 'h4' },
  { name : 'Heading 5', element : 'h5' },
  { name : 'Heading 6', element : 'h6' },
  { name : 'Preformatted Text', element : 'pre' },

  /* Inline Styles */

  { name : 'Inserted Text', element : 'ins' },
  { name : 'Deleted Text', element : 'del' },
  { name: 'Highlight', element : 'p', attributes : { 'class' : 'highlight' } },
  { name: 'Caption', element : 'p', attributes : { 'class' : 'caption' } },

]);
