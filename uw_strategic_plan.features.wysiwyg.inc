<?php

/**
 * @file
 * uw_strategic_plan.features.wysiwyg.inc
 */

/**
 * Implements hook_wysiwyg_default_profiles().
 */
function uw_strategic_plan_wysiwyg_default_profiles() {
  $profiles = array();

  // Exported profile: homepage_body.
  $profiles['homepage_body'] = array(
    'format' => 'homepage_body',
    'editor' => 'ckeditor',
    'settings' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'theme' => 'advanced',
      'language' => 'en',
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Underline' => 1,
          'JustifyLeft' => 1,
          'JustifyCenter' => 1,
          'JustifyRight' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'PasteText' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'Table' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'SpellChecker' => 1,
          'Scayt' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'toolbar_loc' => 'top',
      'toolbar_align' => 'left',
      'path_loc' => 'bottom',
      'resizing' => 1,
      'verify_html' => 1,
      'preformatted' => 0,
      'convert_fonts_to_spans' => 1,
      'remove_linebreaks' => 1,
      'apply_source_formatting' => 0,
      'paste_auto_cleanup_on_paste' => 0,
      'block_formats' => 'p,pre',
      'css_setting' => 'theme',
      'css_path' => '',
      'css_classes' => '',
    ),
    'rdf_mapping' => array(),
  );

  // Exported profile: homepage_centre_text.
  $profiles['homepage_centre_text'] = array(
    'format' => 'homepage_centre_text',
    'editor' => 'ckeditor',
    'settings' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'theme' => 'advanced',
      'language' => 'en',
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Underline' => 1,
          'JustifyLeft' => 1,
          'JustifyCenter' => 1,
          'JustifyRight' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Source' => 1,
          'HorizontalRule' => 1,
          'PasteText' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'SpellChecker' => 1,
          'Scayt' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'toolbar_loc' => 'top',
      'toolbar_align' => 'left',
      'path_loc' => 'bottom',
      'resizing' => 1,
      'verify_html' => 1,
      'preformatted' => 0,
      'convert_fonts_to_spans' => 1,
      'remove_linebreaks' => 1,
      'apply_source_formatting' => 0,
      'paste_auto_cleanup_on_paste' => 0,
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'css_setting' => 'theme',
      'css_path' => '',
      'css_classes' => '',
    ),
    'rdf_mapping' => array(),
  );

  // Exported profile: key_indicators_text.
  $profiles['key_indicators_text'] = array(
    'format' => 'key_indicators_text',
    'editor' => 'ckeditor',
    'settings' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'theme' => 'advanced',
      'language' => 'en',
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Underline' => 1,
          'JustifyLeft' => 1,
          'JustifyCenter' => 1,
          'JustifyRight' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Source' => 1,
          'PasteText' => 1,
          'RemoveFormat' => 1,
          'Styles' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Replace' => 1,
          'SpellChecker' => 1,
          'Scayt' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'imagealign' => array(
          'Image Left' => 1,
          'Image Center' => 1,
          'Image Right' => 1,
        ),
        'clearfloat' => array(
          'Clear Floats' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'toolbar_loc' => 'top',
      'toolbar_align' => 'left',
      'path_loc' => 'bottom',
      'resizing' => 1,
      'verify_html' => 1,
      'preformatted' => 0,
      'convert_fonts_to_spans' => 1,
      'remove_linebreaks' => 1,
      'apply_source_formatting' => 0,
      'paste_auto_cleanup_on_paste' => 0,
      'block_formats' => 'p,pre,h2',
      'css_setting' => 'theme',
      'css_path' => '',
      'css_classes' => '',
    ),
    'rdf_mapping' => array(),
  );

  return $profiles;
}
