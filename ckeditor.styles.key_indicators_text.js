/**
 * @file
 * This file is used/requested by the 'Styles' button.
 *
 * 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 *
 * Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license.
 */

CKEDITOR.addStylesSet('key_indicators_text',
[
  /* Block Styles */

  // These styles are already available in the "Format" combo, so they are
  // not needed here by default. You may enable them to avoid placing the
  // "Format" combo in the toolbar, maintaining the same features.
  { name : 'Paragraph', element : 'p' },
  { name : 'Small', element : 'span', attributes : { 'class' : 'small' } },
  { name : 'Big', element : 'span', attributes : { 'class' : 'big' } },
]);
