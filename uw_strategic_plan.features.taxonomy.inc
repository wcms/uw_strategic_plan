<?php

/**
 * @file
 * uw_strategic_plan.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_strategic_plan_taxonomy_default_vocabularies() {
  return array(
    'uw_tax_themes' => array(
      'name' => 'Theme',
      'machine_name' => 'uw_tax_themes',
      'description' => 'The themes as defined by the strategic plan.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
