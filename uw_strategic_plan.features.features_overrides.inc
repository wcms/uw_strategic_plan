<?php

/**
 * @file
 * uw_strategic_plan.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_strategic_plan_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable.
  $overrides["variable.page_title_default.value"] = '[current-page:page-title] | Strategic plan';
  $overrides["variable.responsive_menu_combined_settings.value|devel"] = array(
    'friendly_name' => 'Development',
    'enabled' => 0,
    'weight' => 0,
    'id' => 'devel',
    'pid' => '',
    'depth' => '',
  );
  $overrides["variable.responsive_menu_combined_settings.value|features"] = array(
    'friendly_name' => 'Features',
    'enabled' => 0,
    'weight' => 1,
    'id' => 'features',
    'pid' => '',
    'depth' => '',
  );
  $overrides["variable.responsive_menu_combined_settings.value|main-menu|enabled"] = 0;
  $overrides["variable.responsive_menu_combined_settings.value|menu-audience-menu|enabled"] = 0;

  return $overrides;
}
